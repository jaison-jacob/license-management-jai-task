import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { routes } from "../router/Routes";
import {Header} from "../component/reusable/index";
import { ThemeProvider } from "@material-ui/core";
import { formTheme } from "../theme/Formtheme";

function Layout() {
  return (
    <ThemeProvider theme={formTheme}>
      <div>
        <Header />
        <Router>
          <Switch>
            {routes.map((item, index) => (
              <Route
                key={index}
                path={item.path}
                component={item.component}
                exact={item.exact}
              />
            ))}
          </Switch>
        </Router>
      </div>
    </ThemeProvider>
  );
}

export default Layout;
