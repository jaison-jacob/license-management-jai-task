import React, { useState, useEffect } from "react";
import { Grid, Typography} from "@material-ui/core";
import { UseForm } from "../../component/reusable/UseForm";
import {
  ButtonComp,
  FormWrapper,
  RadioField,
  SelectField,
  InputField,
  DatePickerField,
} from "../../component/reusable/index";
import FormHeader from "../../component/notificationForm/FormHeader";
import SimpleDialogDemo from "../../component/notificationForm/PopUp";
import {
  initialValue,
  initialValueError,
  copyParameter,
  ApplicableToError,
  changeKey,
  setEditValue,
  setCheckedState,
  handleCheckedState,
} from "../../constants/notification/notificationForm/Form";
import {
  getApplyToFormError,
  mainFormCheckError,
  getErrorFromForm,
} from "../../validation/NotificationForm";
import { useStyles } from "../../pages/notification/Style";
import ApplicableToForm from "../../component/notificationForm/ApplicableToForm";
import {
  getGender,
  getNotificationType,
  getPlan,
  getProduct,
  getUserTYpe,
  PostNotificationType,
  getPromotionOption,
  getNotificationDataById,
  UpdateNotificationType,
} from "../../api/Api";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { useHistory } from "react-router-dom";

function NotificationForm(props) {
  let history = useHistory();

  const { values, handleChange, checkHandle, changeEditValue } =
    UseForm(initialValue);
  const [notificationFormError, setNotificationFormError] =
    useState(initialValueError);
  const [applyToFormCheck, setApplyToFormCheck] = useState("");
  const [applyToError, setapplyToError] = useState(ApplicableToError);
  const [responsibility, setResponsibility] = useState([]);
  const [gender, setGender] = useState([]);
  const [plansOption, setPlansOption] = useState([]);
  const [promotionOption, setpromotionOption] = useState([]);
  const [disable, setdisable] = useState(false);
  const [product, setProduct] = useState([]);
  const [notificationTypeOption, setnotificationTypeOption] = useState([]);
  const [open, setOpen] = React.useState(false);

  const classes = useStyles();

  const handleSubmit = async () => {
    let error = getErrorFromForm(values);
    setNotificationFormError(error);

    let getApplyError = getApplyToFormError(values);

    let isError = false;
    isError = mainFormCheckError(error, isError);

    let ApplyToErrorBoolean = false;
    ApplyToErrorBoolean = mainFormCheckError(
      getApplyError,
      ApplyToErrorBoolean
    );

    ApplyToErrorBoolean === true &&
      setApplyToFormCheck("please enter the Apply to Form correctly");

    //submit form
    if (isError === false && ApplyToErrorBoolean === false) {
      let result = changeKey(values);
      if (typeof history.location.state !== "undefined") {
        await UpdateNotificationType(result, history.location.state.id)
          .then((e) => {
            history.push("/", {});
          })
          .catch((r) => {
            console.log(r);
          });
      } else {
        await PostNotificationType(result).then((res) => {
          history.push("/", {});
        });
      }
    }
  };

  const handleOnchange = (e) => {
    checkHandle(e.target.name, parseInt(e.target.value));
  };

  const handleDate = (date, name) => {
    checkHandle(name, date);
  };

  //get seed from database
  useEffect(() => {
    let editData = {};

    if (typeof history.location.state !== "undefined") {
      history.location.state.status === "View Detail"
        ? setdisable(true)
        : setdisable(false);

      getNotificationDataById(history.location.state.id).then((res) => {
        console.log(res.data)
        editData = setEditValue(res.data);
        changeEditValue(editData);
      });
    }

    getProduct().then((response) => {
      setProduct(response.data);
    });

    getNotificationType().then((response) => {
      setnotificationTypeOption(response.data);
    });

    getUserTYpe().then((response) => {
      let getCheckedState = [];
      getCheckedState = setCheckedState(response.data, editData.userTypes);
      setResponsibility(getCheckedState);
    });

    getGender().then((response) => {
      let getCheckedState = [];
      getCheckedState = setCheckedState(response.data, editData.gender);
      setGender(getCheckedState);
    });

    getPlan().then((response) => {
      setPlansOption(response.data);
    });

    getPromotionOption().then((response) => {
      setpromotionOption(response.data);
    });
  }, []);

  //applyTo form submit
  const applicableSubmit = () => {
    let error = getApplyToFormError(values);
    setapplyToError(error);
    let isError = false;
    isError = mainFormCheckError(error, isError);

    //submit form
    if (isError === false) {
      setApplyToFormCheck("");
      setOpen(false);
    }
  };

  //check box value saved in state
  const checkBoxFun = (e) => {
    let checkValue = [...values[e.target.name]];
    let value = e.target.value;
    handleCheckedState(
      e.target.name,
      value,
      gender,
      responsibility,
      setGender,
      setResponsibility,
      e
    );
    if (e.target.checked) {
      checkValue.push(value);
      checkHandle(e.target.name, checkValue);
    } else {
      checkValue = checkValue.filter((item) => item !== e.target.value);
      checkHandle(e.target.name, checkValue);
    }
  };

  const setClose = () => {
    setOpen(false);
  };

  const notificationFormCancel = () => {
    history.push("/", {});
  };

  return (
    <div style={{ backgroundColor: "#F4F4F4" }}>
      <FormHeader />
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <RadioField
              labelClasses={{root: classes.radioHead,focused:classes.radioFocusLabel }}
              name="notificationTypeId"
              label="Notification Type"
              value={values.notificationTypeId}
              onChange={handleOnchange}
              options={notificationTypeOption}
              radioColor="secondary"
              radioLabel={{
                label: classes.radioLabel,
              }}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12}>
            <SelectField
              variant="filled"
              label="product Name"
              selectLabelStyle={classes.selectLabel}
              value={values.productId}
              onChange={handleChange}
              name="productId"
              color="primary"
              selectOptionStyle={classes.selectOption}
              options={product}
              errorText={notificationFormError.productId}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12}>
            <InputField
              label="Subject"
              value={values.subject}
              onChange={handleChange}
              type="text"
              name="subject"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={classes.selectOption}
              errorText={notificationFormError.subject}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12}>
            <InputField
              label="content"
              value={values.content}
              onChange={handleChange}
              name="content"
              type="text"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={classes.selectOption}
              errorText={notificationFormError.content}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12}>
            <ButtonComp
              endIcon={<ArrowDropDownIcon />}
              onClick={() => setOpen(true)}
              text="Applicable To"
              btnClass={{
                containedPrimary: classes.btnColor,
                root: classes.btnStyle,
              }}
            />
            <SimpleDialogDemo open={open} setOpen={setOpen}>
              <ApplicableToForm
                applicableSubmit={applicableSubmit}
                handleChange={handleChange}
                checkBoxFun={checkBoxFun}
                values={values}
                checkHandle={checkHandle}
                applyToError={applyToError}
                setClose={setClose}
                responsibility={responsibility}
                gender={gender}
                plansOption={plansOption}
                promotionOption={promotionOption}
                disable={disable}
              />
            </SimpleDialogDemo>
          </Grid>
          <Grid item xs={12} style={{ marginTop: 10 }}>
            <div className={classes.line}></div>
          </Grid>
          <Grid item xs={5} style={{ marginTop: 10 }}>
            <Typography style={{ color: "black", marginBottom: 15 }}>
              Scheduler
            </Typography>
            <DatePickerField
              autoOk={true}
              variant="inline"
              inputVariant="filled"
              name="startDate"
              label="start date"
              type="date"
              format="MM/dd/yyyy"
              value={values.startDate}
              helperText={notificationFormError.startDate}
              InputAdornmentProps={{ position: "end" }}
              onChange={handleDate}
              disablePast={true}
              disable={disable}
            />
          </Grid>

          <Grid item xs={4} style={{ marginTop: 49 }}>
            <InputField
              label="Time"
              type="time"
              // onChange={handleChange}
              variant="filled"
              value={values.time}
              name="time"
              textStyle={classes.date}
              shrink={true}
              errorText={notificationFormError.time}
              disable={disable}
            />
          </Grid>
          <Grid item xs={3} style={{ marginTop: 49 }}>
            <InputField
              label="Frequency"
              value={values.frequency}
              onChange={handleChange}
              type="time"
              name="frequency"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={classes.selectOption}
              errorText={notificationFormError.frequency}
              disable={disable}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              label="Increament Time"
              value={values.incrementalTime}
              onChange={handleChange}
              type="time"
              name="incrementalTime"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={classes.date}
              shrink={true}
              errorText={notificationFormError.incrementalTime}
              disable={disable}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              label="Notification Freaquency"
              value={values.notificationPerFreq}
              onChange={handleChange}
              type="Number"
              name="notificationPerFreq"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={classes.selectOption}
              errorText={notificationFormError.notificationPerFreq}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12} style={{ marginTop: 10 }}>
            <div className={classes.line}></div>
          </Grid>
          <Grid item xs={12}>
            <RadioField
              labelClasses={{ root: classes.radioHead }}
              name="copyParameter"
              label="Copy Parameter"
              value={values.copyParameter}
              onChange={handleOnchange}
              options={copyParameter}
              radioColor="secondary"
              radioLabel={{
                label: classes.radioLabel,
              }}
              disable={disable}
            />
          </Grid>
          <Grid item xs={12}>
            {applyToFormCheck && (
              <p style={{ color: "red" }}>{applyToFormCheck}</p>
            )}
            <p style={{ display: "none" }}></p>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <ButtonComp
                onClick={notificationFormCancel}
                text="cancel"
                btnClass={{
                  containedPrimary: classes.applicableApplybtncancel,
                }}
                // disable={disable}
              />
              <ButtonComp
                onClick={handleSubmit}
                text="Submit"
                btnClass={{
                  containedPrimary: classes.applicableApplybtn,
                }}
                disable={disable}
              />
            </div>
          </Grid>
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default NotificationForm;
