import React, { useMemo, useEffect, useState } from "react";
import {Table} from "../../component/reusable/index";
import { COLUMNS } from "../../constants/notification/notificationList/Column";
import {
  useTable,
  useSortBy,
  useGlobalFilter,
  usePagination,
  useBlockLayout,
  useResizeColumns,
} from "react-table";
import { useSticky } from "react-table-sticky";

import { getNotificationData } from "../../api/Api";
// import { set } from 'date-fns';

function List() {
  const [notificationData, setNotificationData] = useState([]);
  const [count, setCount] = useState(null);
  const [pageindex, setPageIndex] = useState(0);

  const getData = async (from, to) => {
    await getNotificationData(from, to).then((res) => {
      setCount(res.data.count);
      setNotificationData(res.data.rows);
    });
  };

  const columns = useMemo(() => COLUMNS(), []);
  const modifiedData = useMemo(() => notificationData, [notificationData]);
  const defaultColumn = React.useMemo(
    () => ({
      minWidth: 30,
      width: 150,
      maxWidth: 400,
    }),
    []
  );
  const tableInstance = useTable(
    {
      columns,
      data: modifiedData,
      defaultColumn,
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    useBlockLayout,
    useSticky,
    useResizeColumns
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canPreviousPage,
    canNextPage,
    pageOptions,
    gotoPage,
    pageCount,
    setPageSize,
    prepareRow,
    state,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter } = state;
  const { pageIndex, pageSize } = state;
  const footerGroups = page.slice(0, pageSize);

  //initially get data from database
  useEffect(() => {
    getData(0, pageSize);
  }, []);
  
  return (
    <div>
      <Table
        getTableProps={getTableProps}
        getTableBodyProps={getTableBodyProps}
        headerGroups={headerGroups}
        page={page}
        nextPage={nextPage}
        previousPage={previousPage}
        canPreviousPage={canPreviousPage}
        canNextPage={canNextPage}
        pageOptions={pageOptions}
        gotoPage={gotoPage}
        pageCount={pageCount}
        setPageSize={setPageSize}
        prepareRow={prepareRow}
        state={state}
        setGlobalFilter={setGlobalFilter}
        tableInstance={tableInstance}
        notificationData={notificationData}
        footerGroups={footerGroups}
        pageIndex={pageIndex}
        pageSize={pageSize}
        globalFilter={globalFilter}
        getData={getData}
        count={count}
        pageindex={pageindex}
        setPageIndex={setPageIndex}
      />
    </div>
  );
}

export default List;
