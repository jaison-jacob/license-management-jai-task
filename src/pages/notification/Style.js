import {
    makeStyles,
  } from "@material-ui/core";

export const useStyles = makeStyles((theme) => {
    console.log(theme);
    return {
      radioHead: {
        color: "#000000DE",
        fontSize: 16,
        fontFamily: "Roboto",
        textAlign: "left",
      },
      radioLabel: {
        color: "#0000008A",
        fontSize: 14,
        fontFamily: "Roboto",
        textAlign: "left",
        opacity: 1,
      },
      radioFocusLabel:{
        color:"black"
      },
      checkLabel: {
        color: "#0000008A",
        fontSize: 14,
        fontFamily: "Roboto",
        textAlign: "left",
        opacity: 1,
        marginRight:-9
      },
      checkFlex:{
        flex:"nowrap"
      },
      radioChecked: {
        color: "#2386A5DE",
      },
      formHeader: {
        display: "flex",
        marginLeft: theme.spacing(3),
      },
      formHeaderText: {
        color: "#000000DE",
        fontFamily: "Rubik",
        fontSize: 20,
        marginLeft: theme.spacing(1),
        fontWeight: 600,
      },
      formControl: {
        color: "green",
      },
      marginBottom: {
        marginBottom: theme.spacing(3),
      },
      selectLabel: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Roboto",
        fontWeight: 200,
      },
      selectOption: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Roboto",
      },
      inputFocused: {
        color: "red",
      },
      chip: {
        backgroundColor: "white",
        borderRadius: 16,
        fontSize: 14,
        color: "#0000008A",
        fontFamily: "Lato",
      },
      date: {
        width: "100%",
        height: 19,
        fontSize: 12,
      },
      line:{
        width:"100%",
        height:1,
        backgroundColor:"#00000033",
        boxShadow:"0px 1px 3px #00000033",
        opacity:1
      },
      applicableApplybtn:{
        backgroundColor:"#1976D2",
        fontFamily:"Roboto",
        marginLeft:theme.spacing(2)
      },
      applicableApplybtncancel:{
        backgroundColor:"#FFFFFF",
        color:"#0000008A",
        fontFamily:"Roboto"
      },
      ApplicableFormContainer: {
        width: "95%",
        marginLeft: theme.spacing(1),
        marginTop: theme.spacing(2),
      },
      error:{
        color:"red"
      },
  btnStyle: {
    height: 42,
    width: "30%",
    fontFamily: "Roboto",
    fontSize: 16,
    letterSpacing: 0,
    textTransform: "capitalize",
    border: "1px solid #B4D9FF",
    "&:hover": {
      backgroundColor: "#DBEDFF",
    },
  },
  btnColor: {
    color: "#909090",
    backgroundColor: "#DBEDFF",
  },
     
    };
  });