import axios from "axios";
import { Url } from "./Urls";

export const appApi = axios.create({
  baseURL: "http://localhost:4000/",
  headers: { Authorization: `Bearer abc` },
});

export const getProduct = () => appApi.get(Url.GET_PRODUCTS);
export const getGender = () => appApi.get(Url.GET_GENDER);
export const getUserTYpe = () => appApi.get(Url.GET_USERTYPE);
export const getPlan = () => appApi.get(Url.GET_PLANS);
export const getNotificationType = () => appApi.get(Url.GET_NOTIFICATIONTYPE);
export const getPromotionOption = () => appApi.get(Url.GET_PROMOTION);
// export const getNotificationData = (from,to) =>
//   appApi.post(Url.GET_NOTIFICATIONDETAILS+`${to}/${from}`);
  export const getNotificationData = (from,to) =>
  appApi.post(Url.GET_NOTIFICATIONDETAILS+`${to}/${from}`);
export const getNotificationDataById = (id) =>
  appApi.get(Url.GET_NOTIFICATIONDETAILSBYID + `${id}`);

export const PostNotificationType = (data) =>
  appApi.post(Url.POST_NOTIFICATION, data);
export const UpdateNotificationType = (data, id) =>
  appApi.put(Url.UPDATE_NPTIFICATION + `${id}`, data);
