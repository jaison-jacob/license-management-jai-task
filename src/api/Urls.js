export const Url = {
    GET_PRODUCTS: "getAllDetails/products",
    GET_GENDER: "getAllDetails/genderDetails",
    GET_USERTYPE: "getAllDetails/userTypes",
    GET_PLANS:"getAllDetails/plan",
    GET_NOTIFICATIONTYPE:"getAllDetails/notifyType",
    GET_PROMOTION:"getAllDetails/promotions",

    GET_NOTIFICATIONDETAILS:"getAllDetailsWithCount/notifications/",
    GET_NOTIFICATIONDETAILSBYID:"getDetailById/notifications/",

    POST_NOTIFICATION:"createDetails/notifications",
    UPDATE_NPTIFICATION:"updateDetails/notifications/"
}