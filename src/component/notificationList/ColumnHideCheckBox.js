import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popper from "@material-ui/core/Popper";
import CheckboxList from "./CheckboxList";
import { IconButton } from "@material-ui/core";
import SortIcon from "@material-ui/icons/Sort";

const useStyles = makeStyles((theme) => ({
  paper: {
    border: "1px solid",
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
  },
  popperStyle: {
    marginTop: 20,
    marginLeft: 120,
  },
  sortIconStyle: {
    fontSize: 16,
    color: "#0000008A",
  },
}));

export default function SimplePopper() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <div>
      <IconButton onClick={handleClick}>
        <SortIcon className={classes.sortIconStyle} />
      </IconButton>

      <Popper
        id={id}
        open={open}
        anchorEl={anchorEl}
        className={classes.popperStyle}
      >
        <CheckboxList />
      </Popper>
    </div>
  );
}
