import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import clsx from "clsx";
import "./style/OrganizationTable.scss";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "240px",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    boxShadow: "0px 10px 20px #00000029",
    color: "#707070",
  },
  marginLeft: {
    marginLeft: theme.spacing(6),
  },
}));

const option = [
  "Email",
  "Contact No",
  "Profession",
  "Jockey Cattagory",
  "Login With",
];

export default function CheckboxList() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([0]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <div className={classes.marginLeft}>
      <List className={clsx(classes.root, "checkBoxContainer")}>
        <div className="checkBoxHeader">
          <div className="checkBoxHeaderTextContainer">
            <p className="checkBoxHeaderTextheading">Note</p>
            <p className="checkBoxHeaderTextcontent">
              Select the check bo to hide and unhide table column
            </p>
          </div>
        </div>
        {option.map((value, ind) => {
          const labelId = `checkbox-list-label-${value}`;

          return (
            <ListItem
              key={ind}
              role={undefined}
              dense
              button
              onClick={handleToggle(value)}
            >
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ "aria-labelledby": labelId }}
                  color="primary"
                />
              </ListItemIcon>
              <ListItemText
                id={labelId}
                primary={value}
                className="checkboxText"
              />
            </ListItem>
          );
        })}
      </List>
    </div>
  );
}
