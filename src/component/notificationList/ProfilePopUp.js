import React from "react";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    width: 230,
  },
});

const options = ["View Detail", "Edit", "Inactive"];

const ITEM_HEIGHT = 48;

function ProfilePopUp(props) {
  let history = useHistory();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const l = async (index, type) => {
    let state = { id: index, status: type };
    await history.push("/form", state);
  };

  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: "180px",
            fontSize: 2,
          },
        }}
      >
        {options.map((option) => (
          <MenuItem
            key={props.index}
            selected={option === "Pyxis"}
            onClick={handleClose}
            className="profilePopupIteme"
            onClick={() => l(props.index, option)}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}

export default ProfilePopUp;
