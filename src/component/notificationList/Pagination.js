import React from "react";
import TablePagination from "@material-ui/core/TablePagination";

export default function Pagination({
  pageIndex,
  nextPage,
  previousPage,
  setPageSize,
  pageSize,
  pageOptions,
  gotoPage,
  getData,
  count,
  pageindex,
  setPageIndex,
}) {
  const handleChangePage = async (event, newPage) => {
    await getData(newPage * pageSize, pageSize);
    if (pageindex > newPage) {
      setPageIndex(pageindex - 1);
    } else {
      setPageIndex(pageindex + 1);
    }
  };

  const handleChangeRowsPerPage = async (event) => {
    getData(0, event.target.value);
    setPageIndex(0);
    setPageSize(parseInt(event.target.value, 10));
  };
  return (
    <TablePagination
      component="div"
      count={count}
      page={pageindex}
      onChangePage={handleChangePage}
      rowsPerPage={pageSize}
      onChangeRowsPerPage={handleChangeRowsPerPage}
      rowsPerPageOptions={[2, 5, 10, 25]}
    />
  );
}
