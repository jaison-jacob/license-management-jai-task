import React from "react";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import { makeStyles, Typography, Button } from "@material-ui/core";
import FilterListIcon from "@material-ui/icons/FilterList";
import GetAppIcon from "@material-ui/icons/GetApp";
import { Link } from "react-router-dom";
import { dashboardPath } from "../../router/DashboardPath";
const useStyles = makeStyles({
  underline: {
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  branch_Text: {
    fontFamily: "Lato",
    fontSize: 20,
    textAlign: "left",
    color: "#333333",
  },
  filter_button: {
    width: 98,
    height: 42,
    backgroundColor: "#E2E2E2",
    color: "#000000DE",
    fontSize: 14,
    fontFamily: "Rubik",
    textTransform: "capitalize",
  },
  new_button: {
    width: 85,
    height: 42,
    backgroundColor: "#59B961",
    color: "#FFFFFF",
    fontSize: 16,
    textTransform: "capitalize",
  },
  download_button: {
    width: 42,
    height: 42,
    paddingLeft: "5%",
    backgroundColor: "#264DB5",
    color: "#FFFFFF",
    textTransform: "capitalize",
  },
  tableNavContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    height: 70,
    width: 1000,
    marginTop: 30,
  },
  tableNavButtonContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  tableNavButtons: {
    display: "flex",
    justifyContent: "space-between",
    width: 525,
  },
  tableNavSearchBox: {
    width: 240,
    height: 42,
    backgroundColor: "#F4F4F4",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

function GlobalFilter({ filter, setFilter }) {
  const classes = useStyles();
  return (
    <div className={classes.tableNavContainer}>
      <Typography component="h6" className={classes.branch_Text}>
        Branches
      </Typography>
      <div className={classes.tableNavButtonContainer}>
        <div className={classes.tableNavButtons}>
          <div className={classes.tableNavSearchBox}>
            <TextField
              id="standard-start-adornment"
              value={filter || ""}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
                classes,
              }}
              onChange={(e) => setFilter(e.target.value)}
              size="small"
            />
          </div>
          <Button
            variant="contained"
            startIcon={<FilterListIcon />}
            className={classes.filter_button}
          >
            Filter
          </Button>
          
          <Link
            to={dashboardPath.NOTIFICATIONFORM}
            style={{ textDecoration: "none" }}
          >
            <Button variant="contained" className={classes.new_button}>
              New
            </Button>
          </Link>
          <Button
            variant="contained"
            startIcon={<GetAppIcon style={{ margin: "0 auto" }} />}
            className={classes.download_button}
          ></Button>
        </div>
      </div>
    </div>
  );
}

export default GlobalFilter;
