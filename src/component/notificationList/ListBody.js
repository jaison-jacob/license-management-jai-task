import React from "react";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import "./style/OrganizationTable.scss";

function ListHeader(props) {
  return (
    <div>
      <div className="header">
        {props.headerGroups.map((headerGroup) => (
          <div {...headerGroup.getHeaderGroupProps()} className="tr">
            {headerGroup.headers.map((column) => (
              <div
                style={{
                  left: column.sticky === "left" ? "0px" : "unset",
                  right: column.sticky === "right" ? "0px" : "unset",
                }}
                {...column.getHeaderProps(column.getSortByToggleProps())}
                className="th"
              >
                {column.render("Header")}

                <div {...column.getResizerProps()} className="resizer">
                  <DragIndicatorIcon style={{ fontSize: 15 }} />
                </div>
                <span style={{ float: "right" }}>
                  {column.isSorted ? (
                    column.isSortedDesc ? (
                      <i
                        class="fa fa-caret-down"
                        aria-hidden="true"
                        color="white"
                      ></i>
                    ) : (
                      <i class="fa fa-caret-up" aria-hidden="true"></i>
                    )
                  ) : (
                    ""
                  )}
                </span>
              </div>
            ))}
          </div>
        ))}
      </div>
      <div {...props.getTableBodyProps()} className="body">
        {props.footerGroups.map((row) => {
          props.prepareRow(row);
          return (
            <div {...row.getRowProps()} className="tr">
              {row.cells.map((cell) => (
                <div {...cell.getCellProps()} className="td">
                  {cell.render("Cell")}
                </div>
              ))}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default ListHeader;
