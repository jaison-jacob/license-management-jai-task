import React from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import { DialogContent, Typography, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useStyles } from "./style/popUpStyle";

export default function SimpleDialogDemo(props) {
  const classes = useStyles();
  const { children, setOpen, open } = props;

  return (
    <div>
      <Dialog
        onClose={() => setOpen(false)}
        aria-labelledby="simple-dialog-title"
        open={open}
        classes={{ paper: classes.dialogStyle }}
      >
        <DialogTitle
          id="simple-dialog-title"
          classes={{ root: classes.dialogTittleContainer }}
        >
          <Typography className={classes.dialogTittle}>
            Applicable To
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => props.setOpen(false)}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>{children}</DialogContent>
      </Dialog>
    </div>
  );
}
