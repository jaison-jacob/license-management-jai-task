import React from "react";
import { IconButton, Typography } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useStyles } from "./style/FormHeaderStyle";
import { useHistory } from "react-router-dom";

function FormHeader() {
  const styles = useStyles();
  let history = useHistory();
  return (
    <div>
      <div className={styles.formHeader}>
        <IconButton
          aria-label="delete"
          className={styles.formHeaderIcon}
          size="medium"
          onClick={() => history.push("/", {})}
        >
          <ArrowBackIcon fontSize="inherit" />
        </IconButton>
        <Typography className={styles.formHeaderText}>
          New Notification
        </Typography>
      </div>
    </div>
  );
}

export default FormHeader;
