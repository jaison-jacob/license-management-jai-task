import React from "react";
import { pincodeOption } from "../../constants/notification/notificationForm/Form";
import { Grid } from "@material-ui/core";
import {ButtonComp,InputField,AutoCompleteField,CheckBoxFeild} from "../reusable/index"
import { useStyles } from "../../pages/notification/Style";

function ApplicableToForm(props) {
  const {
    applicableSubmit,
    handleChange,
    checkBoxFun,
    values,
    checkHandle,
    applyToError,
    responsibility,
    gender,
    plansOption,
    promotionOption,
    disable,
  } = props;
  const classes = useStyles();
  
  return (
    <div className={classes.ApplicableFormContainer}>
      <Grid container spacing={4}>
        <Grid item xs={5}>
          <InputField
            name="ageFrom"
            fullWidth={true}
            type="Number"
            variant="filled"
            textStyle={classes.selectOption}
            label="age Limit From"
            onChange={handleChange}
            value={values.ageFrom}
            errorText={applyToError.ageFrom}
            disable={disable}
          />
        </Grid>
        <Grid item xs={5}>
          <InputField
            name="ageTo"
            fullWidth={true}
            type="Number"
            variant="filled"
            color="primary"
            textStyle={classes.selectOption}
            label="age Limit To"
            onChange={handleChange}
            value={values.ageTo}
            errorText={applyToError.ageTo}
            disable={disable}
          />
        </Grid>
        <Grid item xs={12}>
          <AutoCompleteField
            options={pincodeOption}
            value={values.pincodes}
            style={{ outlined: classes.chip }}
            chipVariant="outlined"
            textVariant="filled"
            label="pincode"
            name="pincodes"
            item={true}
            onChange={checkHandle}
            error={applyToError}
            errorText={applyToError.pincodes}
            disable={disable}
          />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={7} style={{ margin: "30px 0" }}>
          <CheckBoxFeild
            checkBoxHead={{ root: classes.radioHead }}
            label="Assign responsibility"
            options={responsibility}
            value={values.userTypes}
            onChange={checkBoxFun}
            checkLabel={{ label: classes.checkLabel }}
            name="userTypes"
            errorText={applyToError.userTypes}
            disable={disable}
          />
        </Grid>
        <Grid item xs={5} style={{ margin: "30px 0" }}>
          <CheckBoxFeild
            checkBoxHead={{ root: classes.radioHead }}
            label="Gender"
            value={values.gender}
            options={gender}
            onChange={checkBoxFun}
            checkLabel={{ label: classes.checkLabel }}
            name="gender"
            errorText={applyToError.gender}
            disable={disable}
          />
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <AutoCompleteField
            options={plansOption}
            item={false}
            value={values.plans}
            style={{ outlined: classes.chip }}
            chipVariant="outlined"
            textVariant="filled"
            label="plans"
            name="plans"
            onChange={checkHandle}
            errorText={applyToError.plans}
            disable={disable}
          />
        </Grid>
        <Grid item xs={12}>
          <AutoCompleteField
            options={promotionOption}
            item={true}
            value={values.promotions}
            style={{ outlined: classes.chip }}
            chipVariant="outlined"
            textVariant="filled"
            label="promotions"
            name="promotions"
            onChange={checkHandle}
            errorText={applyToError.promotions}
            disable={disable}
          />
        </Grid>
        <Grid item xs={12}>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <ButtonComp
              onClick={props.setClose}
              text="cancel"
              btnClass={{
                containedPrimary: classes.applicableApplybtncancel,
                root: classes.btnStyle,
              }}
            />
            <ButtonComp
              onClick={applicableSubmit}
              text="Apply"
              btnClass={{
                containedPrimary: classes.applicableApplybtn,
                root: classes.btnStyle,
              }}
            />
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default ApplicableToForm;
