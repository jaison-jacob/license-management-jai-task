import React, { useState } from 'react'

export function UseForm(initialValue) {
    const [values,setValue] = useState(initialValue)
    const handleChange = (e) => {
        console.log(e)
        const {name,value} = e.target
        
            setValue({...values,[name]:value})
    }
    const checkHandle = (name,value) => {
        console.log(name,value)
        setValue({...values,[name]:value})
    }
    const handleDelete = (dta) => {
        console.log(dta);
        setValue((chips) => chips.filter((chip) => chip !== dta));
      };
      const changeEditValue = (data) => {
          setValue(data)
      }
    return {
        values,setValue,handleChange,handleDelete,checkHandle,changeEditValue
    }
}


