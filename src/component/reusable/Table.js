import React, { useMemo, useEffect, useState } from "react";
import "../notificationList/style/OrganizationTable.scss";
import GlobalFilter from "../notificationList/GlobalFilter";
import Pagination from "../notificationList/Pagination";
import ListBody from "../notificationList/ListBody";
import "../notificationList/style/OrganizationTable.scss";

function Table(props) {
  const {
    globalFilter,
    setGlobalFilter,
    getTableProps,
    headerGroups,
    getTableBodyProps,
    footerGroups,
    prepareRow,
    pageIndex,
    pageCount,
    nextPage,
    previousPage,
    setPageSize,
    pageSize,
    pageOptions,
    state,
    getData,
    count,
    gotoPage,
    setPageIndex,
    pageindex,
  } = props;
  console.log(pageIndex);

  if (!props.notificationData.length) {
    return (
      <div className="tableContainer">
        <div>
          <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
        </div>
        <p>no dsta found</p>
      </div>
    );
  }

  return (
    <div className="tableContainer">
      <div>
        <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      </div>

      <div>
        <div
          {...getTableProps()}
          className="table sticky"
          style={{ width: 1000, height: 400 }}
        >
          <ListBody
            headerGroups={headerGroups}
            getTableBodyProps={getTableBodyProps}
            footerGroups={footerGroups}
            prepareRow={prepareRow}
          />
        </div>
      </div>
      <div className="pagination_container">
        <Pagination
          pageIndex={pageIndex}
          pageCount={pageCount}
          nextPage={nextPage}
          previousPage={previousPage}
          setPageSize={setPageSize}
          pageSize={pageSize}
          pageOptions={pageOptions}
          getData={getData}
          count={count}
          gotoPage={gotoPage}
          setPageIndex={setPageIndex}
          pageindex={pageindex}
        />
      </div>
    </div>
  );
}
export {Table};
