import React from "react";
import Button from "@material-ui/core/Button";

function ButtonComp(props) {
  const { btnClass,text, onclick, variant, color,disable, ...others } = props;
  return (
    <div>
      <Button
        variant={variant || "contained"}
        color={color || "primary"}
        onClick={onclick}
        classes={btnClass}
        size="medium"
        disabled={disable}
        {...others}
      >
        {text}
      </Button>
    </div>
  );
}

export {ButtonComp};
