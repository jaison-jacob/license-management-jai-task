import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    formContainer:{
        width:"52.12%",
        color:"#FFFFFF",
        backgroundColor:"#FFFFFF",
        margin:"0 auto",
        padding:"5%",
        boxShadow:"0px 1px 3px #00000033",
        zIndex:100
    }
}))

function FormWrapper(props) {
    const classes = useStyles()
    return (
        <div className={classes.formContainer}>
            {props.children}
        </div>
    )
}

export {FormWrapper}
