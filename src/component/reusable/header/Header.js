import React from 'react'
import {Grid,makeStyles} from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    headerContainer:{
        height:64,
        backgroundColor:"#2C56C6",
        color:"#FFFFFF",
        fontSize:16,
        display:"flex",
        alignItems:"center",
        paddingLeft:64,
        fontFamily:"Rubik"
    }
}))

function Header() { 
    const classes = useStyles()
    return (
        <div>
            <Grid container>
                <Grid item xs={12} className={classes.headerContainer}>
                    Notification
                </Grid>
            </Grid>
        </div>
    )
}

export {Header}
