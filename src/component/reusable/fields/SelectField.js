import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from "@material-ui/core";

function SelectField(props) {
  return (
    <div>
      <FormControl
        variant="filled"
        fullWidth
        error={Boolean(props.errorText)}
        disabled={props.disable}
      >
        <InputLabel className={props.selectLabelStyle}>
          {props.label}
        </InputLabel>
        <Select
          value={props.value}
          onChange={props.onChange}
          name={props.name}
          color={props.color}
          className={props.selectOptionStyle}
        >
          {/* change value e.name to e.id, change optiion to options */}
          {props.options.map((e, i) => (
            <MenuItem value={e.id} key={i}>
              {e.name}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{props.errorText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export {SelectField};
