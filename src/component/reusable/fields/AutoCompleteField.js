import React from "react";
import { Chip, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

function AutoCompleteField(props) {
  return (
    <div>
      <Autocomplete
        multiple
        disabled={props.disable}
        id="tags-filled"
        options={props.options}
        getOptionLabel={(option) => option.name}
        value={props.value}
        onChange={(e, newValue, l) => {
          props.onChange(props.name, newValue);
        }}
        name={props.name}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              key={index}
              classes={props.style}
              variant={props.chipVariant || "outlined"}
              label={option.name}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant={props.textVariant || "filled"}
            label={props.label}
            name={props.name}
            error={Boolean(props.errorText)}
            helperText={props.errorText}
          />
        )}
      />
    </div>
  );
}

export { AutoCompleteField };
