import React from "react";
import { TextField } from "@material-ui/core";

function InputField(props) {
  return (
    <div>
      <TextField
        id="filled-basic"
        label={props.label}
        disabled={props.disable}
        type={props.type || "text"}
        variant={props.variant || "filled"}
        onChange={props.onChange}
        value={props.value}
        name={props.name}
        inputProps={{
          className: props.textStyle,
        }}
        InputLabelProps={{
          shrink: props.shrink,
        }}
        fullWidth
        error={Boolean(props.errorText)}
        helperText={props.errorText}
      />
    </div>
  );
}

export {InputField};
