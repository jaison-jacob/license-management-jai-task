import React from "react";
import {
  DatePicker,
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib

function DatePickerField(props) {
  return (
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <DatePicker
          autoOk={props.autoOk}
          variant={props.variant}
          error={Boolean(props.helperText)}
          inputVariant={props.inputVariant}
          name={props.name}
          helperText={props.helperText}
          fullWidth
          label={props.label}
          format={props.format}
          value={props.value}
          onChange={(date) => props.onChange(date, props.name)}
          disablePast={props.disablePast}
          disabled={props.disable}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
}

export {DatePickerField};
