import React from "react";
import {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";

function RadioField(props) {
  return (
    <div>
      {/* change option array to array of object with id,name , change optiion to options */}
      <FormControl component="fieldset">
        <FormLabel component="legend" classes={{...props.labelClasses}} focused={false}>
          {props.label}
        </FormLabel>
        <RadioGroup
          name={props.name}
          value={Number(props.value)}
          onChange={props.onChange}
          row
        >
          {props.options.map((e, i) => (
            <FormControlLabel
              key={i}
              value={e.id}
              control={<Radio size="small" color={props.color}/>}
              label={e.name || ""}
              classes={{...props.radioLabel}}
              disabled={props.disable}
            />
          ))}
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export {RadioField};
