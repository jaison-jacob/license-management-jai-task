import React from "react";
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormHelperText,
} from "@material-ui/core";

function CheckBoxFeild(props) {
  return (
    // change the hard coded label to dynamic props label,  change the value to e.id
    <div>
      <FormControl component="fieldset" error={!!props.errorText}>
        <FormLabel component="legend" focused={false} classes={props.checkBoxHead}>
          {props.label}
        </FormLabel>
        <FormGroup row>
          {props.options.map((e,index) => (
            <FormControlLabel
            key={index}
              control={
                <Checkbox
                  checked={e.checked}
                  size="small"
                  value={e.id}
                  onChange={props.onChange}
                  name={props.name}
                  key={index}
                />
              
              }
              label={e.name}
              classes={props.checkLabel}
              disabled={props.disable}
            />
          ))}
        </FormGroup>
        <FormHelperText>{props.errorText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export {CheckBoxFeild};
