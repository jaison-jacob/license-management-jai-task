import {
  PostNotificationType,
  getNotificationDataById,
  UpdateNotificationType,
} from "../../api/Api";
import {
  initialValue,
  ApplicableTo,
} from "../../constants/notification/notificationForm/Form";

let initialState = {
  notificationData: {},
  edit: {},
  id: null,
};
export const CHANGE_STATE = "CHANGE_STATE";
export function appOnchange(name, value) {
  return { type: CHANGE_STATE, payload: { name: name, value: value } };
}

export const notificationFormSubmit = (data) => {
  console.log("call");
  return async (dispatch, getState) => {
    let copybranchListData = {
      ...getState().notificationListReducer.notificationData,
    };
    let combine = { ...copybranchListData, ...data };
    console.log(combine);
    await PostNotificationType(combine).then((res) => {
      console.log(res);
    });
    dispatch(appOnchange("notificationData", combine));
  };
};

export const notificationApplyToFormSubmit = (data) => {
  return async (dispatch, getState) => {
    dispatch(appOnchange("notificationData", data));
  };
};
// {
//   "notificationTypeId": 1,
//   "productId": 1,
//   "subject": "subject",
//   "content": 1,
//   "startDate": "2021-03-21",
//   "frequency": "00:00:10",
//   "notificationPerFreq": 10,
//   "incrementalTime": "00:01:10",
//   "copyParameter": 100,
//   "ageFrom": 10,
//   "ageTo": 20,
//   "pincodes": [{"pincode": "628198"}, {"pincode": "672387"}],
//   "userTypes": [
//     {"userTypeId": 1}, {"userTypeId": 2}],
//   "gender": [{"genderId": 1}, {"genderId": 2}],
//   "plans": [{"planId": 1}],
//   "promotions": [{"promotionId": 1}]
// }

export const notificationFormEdit = (id) => {
  console.log(id);
  return async (dispatch, getState) => {
    // console.log(Url.GET_NOTIFICATIONDETAILSBYID+`${id}`)
    await getNotificationDataById(id).then((res) => {
      console.log(res);

      let data = setEditValue(res.data);
      console.log(data)
      dispatch(appOnchange("edit", data));
      dispatch(appOnchange("id", id));
    });
  };
};
export const notificationFormUpdate = (data) => {
  return async (dispatch, getState) => {
    // console.log(Url.GET_NOTIFICATIONDETAILSBYID+`${id}`)
    let id = getState().notificationListReducer.id;
    UpdateNotificationType(data, id)
      .then((e) => {
        console.log(e);
      })
      .catch((ek) => {
        console.log(ek);
      });
    dispatch(appOnchange("edit", {}));
    dispatch(appOnchange("id", null));
  };
};

const setEditValue = (value) => {
  let createValue = {};
  for (const item in initialValue) {
    if (item === "pincodes") {
      let a = value[item].map((e) => ({ name: e.pincode }));
      console.log(a);
      createValue = { ...createValue, [item]: a };
    } else if(item === "gender") {
      createValue = { ...createValue, [item]: [value[item]] };
    }else {
      createValue = { ...createValue, [item]: value[item] };
    }
  }
  console.log(createValue);
  return createValue;
};

//  const setUpdateValue = (value) => {
//   let createValue = {"copyParameter":2};
//   console.log(initialValue)
//   for (const item in initialValue) {
//    if(item !== "pincodes"&&
//    item !== "userTypes"&&
//    item !== "gender"&&
//    item !== "plans"&&
//    item !== "promotions"
//    ){
//      console.log(value[item])
//     createValue = {...createValue,[item]:value[item]}
//    }

//   }
//   console.log(createValue)
//   return createValue
// }

export const notificationListReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_STATE:
      return { ...state, [action.payload.name]: action.payload.value };
    default:
      return state;
  }
};
