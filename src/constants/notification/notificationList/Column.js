// import { Link } from 'react-router-dom';
// import {todo_Edit,todo_Delete} from '../../redux/action'
import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import SortIcon from "@material-ui/icons/Sort";
import { Box } from "@material-ui/core";
import { format } from "date-fns";
// import imagepatt from "../asserts/Ellipse 1.png"
// import DOTT from "./assets/Group 1794 (1).svg";
import PrfilePopUp from "../../../component/notificationList/ProfilePopUp";
// import "../../../  /OrganizationTable.scss"
import "../../../component/notificationList/style/OrganizationTable.scss";
import ColumnHideCheckBox from "../../../component/notificationList/ColumnHideCheckBox";
// import classes from "*.module.css";

export const COLUMNS = () => [
  {
    Header: () => (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          zIndex: 100,
        }}
      >
        <ColumnHideCheckBox />

        <Typography
          component="h6"
          style={{ fontSize: 14, margin: "0 auto", marginLeft: "30%" }}
        >
          ProductName
        </Typography>
      </div>
    ),
    accessor: "product",
    sticky: "left",
    width: 346,
    minWidth: 200,
    maxWidth: 450,
    Cell: (orgin) => {
     
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            color: "#000000DE",
            width: "100%",
          }}
        >
          <div style={{ display: "flex", alignItems: "center", width: "40%" }}>
            {/* <Avatar src={imagepatt} style={{ width: 32, height: 32 }} /> */}
            <Typography
              component="h6"
              style={{ fontSize: 14, marginLeft: "30%" }}
            >
              {orgin.cell.value.name}
            </Typography>
          </div>
          <PrfilePopUp index={orgin.cell.row.original.id} />
        </div>
      );
    },
  },
  {
    Header: "subject",
    accessor: "subject",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
  },
  {
    Header: "content",
    accessor: "content",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  {
    Header: "createdAt",
    accessor: "createdAt",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
  },
  {
    Header: "frequency",
    accessor: "frequency",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "green",
  },
  {
    Header: "startDate",
    accessor: "startDate",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },

  {
    Header: "ageFrom",
    accessor: "ageFrom",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  {
    Header: "ageTo",
    accessor: "ageTo",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  // {
  // 	Header: "Longitude",
  // 	accessor: "Longitude",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "LandMark",
  // 	accessor: "LandMark",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Distance From LandMark",
  // 	accessor: "distanceFromLandMark",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Nearest",
  // 	accessor: "Nearest",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
  // {
  // 	Header: "Effective Date",
  // 	accessor: "effectiveDate",
  // 	Cell: ({ value }) => {
  // 		return format(new Date(value), "dd/MM/yyyy")
  // 	},
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // },
  // {
  // 	Header: "Reason For Deactive",
  // 	accessor: "reasonForDeactive",
  // 	width: 250,
  // 	minWidth: 200,
  // 	maxWidth: 300,
  // 	color: "#000000B8",
  // },
];
