import React from "react";

export const notificationFormProductNameOption = [
  {
    id: 1,
    name: "oil",
  },
  {
    id: 2,
    name: "Bag",
  },
  {
    id: 3,
    name: "laptop",
  },
  {
    id: 4,
    name: "phone",
  },
];
export const pincodeOption = [
  {
    name: "601989",
  },
  {
    name: "601984",
  },
  {
    name: "601932",
  },
  {
    name: "601982",
  },
  {
    name: "601981",
  },
  {
    name: "601248",
  },
];
export const plans = [
  {
    id: 1,
    name: "siver",
  },
  {
    id: 2,
    name: "Gold",
  },
  {
    id: 3,
    name: "platinum",
  },
];
export const promotors = [
  {
    id: 1,
    name: "FREE",
  },
  {
    id: 2,
    name: "OFF10",
  },
  {
    id: 3,
    name: "OFF08",
  },
];

export const notificationType = [
  {
    id: 1,
    name: "Mobile app",
  },
  {
    id: 2,
    name: "Email",
  },
  {
    id: 3,
    name: "SMS",
  },
];

export const copyParameter = [
  {
    id: 100,
    name: "cc",
  },
  {
    id: 200,
    name: "bcc",
  },
];

export const userType = [
  {
    id: 1,
    name: "Individual",
  },
  {
    id: 2,
    name: "Team",
  },
  {
    id: 3,
    name: "Company",
  },
];

export const Gender = [
  {
    id: 1,
    name: "Male",
  },
  {
    id: 2,
    name: "Female",
  },
  {
    id: 3,
    name: "both",
  },
];

export const ApplicableTo = {
  ageFrom: "",
  ageTo: "",
  pincodes: [
    {
      name: "601989",
    },
  ],
  userTypes: [],
  gender: [],
  plans: [],
  promotions: [],
};
export const ApplicableToError = {
  ageFrom: "",
  ageTo: "",
  pincodes: "",
  userTypes: "",
  gender: "",
  plans: "",
  promotions: "",
};

export const initialValue = {
  notificationTypeId: 1,
  productId: 1,
  subject: "",
  content: "",
  startDate: new Date(),
  // time: "",
  frequency: "",
  incrementalTime: "",
  notificationPerFreq: "",
  copyParameter: 200,
  ageFrom: "",
  ageTo: "",
  pincodes: [
    {
      name: "601989",
    },
  ],
  userTypes: [],
  gender: [],
  plans: [],
  promotions: [],
};
export const initialValueError = {
  subject: "",
  content: "",
  startDate: "",
  // time: "",
  frequency: "",
  increamentTime: "",
  incrementalTime: "",
  notificationPerFreq: "",
  copyParameter: "",
};

export const changeKey = (data) => {
  let values = { ...data };

  values["pincodes"].forEach((item, i) => {
    values["pincodes"][i] = renameKey(item.name, "pincode");
  });
  values["plans"].forEach((item, i) => {
    values["plans"][i] = renameKey(item.id, "planId");
  });
  values["promotions"].forEach((item, i) => {
    values["promotions"][i] = renameKey(item.id, "promotionId");
  });

  values["userTypes"].forEach((item, i) => {
    values["userTypes"][i] = renameKey(item, "userTypeId");
  });
  values["gender"].forEach((item, i) => {
    values["gender"][i] = renameKey(item, "genderId");
  });
  return values;
};

const renameKey = (value, newKey) => {
  return { [newKey]: value };
};

export const setEditValue = (value) => {
  let createValue = {};
  for (const item in initialValue) {
    if (item === "pincodes") {
      let a = value[item].map((e) => ({ name: e.pincode }));
      createValue = { ...createValue, [item]: a };
    } else if (item === "gender") {
      createValue = { ...createValue, [item]: [value[item].id] };
    } else if (item === "userTypes") {
      let a = value[item].map((e) => e.id);
      createValue = { ...createValue, [item]: a };
    } else {
      createValue = { ...createValue, [item]: value[item] };
    }
  }
  return createValue;
};

export const setCheckedState = (data, edit) => {
  if (typeof edit !== "undefined") {
    data = data.map((item, i) => {
      return isCheckedState(edit, item.id)
        ? { ...data[i], checked: true }
        : { ...data[i], checked: false };
    });
  } else {
    data = data.map((item, i) => ({ ...data[i], checked: false }));
  }
  console.log(data);
  return data;
};

const isCheckedState = (edit, id) => {
  for (let i = 0; i < edit.length; i++) {
    if (Number(edit[i]) === Number(id)) {
      return true;
    }
  }
  return false;
};

export const handleCheckedState = (
  name,
  value,
  gender,
  responsibility,
  setGender,
  setResponsibility,
  e
) => {
  if (name === "gender") {
    let check = gender.map((item, i) =>
      Number(value) === Number(item.id)
        ? { ...gender[i], checked: e.target.checked }
        : { ...gender[i] }
    );
    setGender(check);
  } else {
    let check = responsibility.map((item, i) => {
      return Number(value) === Number(item.id)
        ? { ...responsibility[i], checked: e.target.checked }
        : { ...responsibility[i] };
    });

    setResponsibility(check);
  }
};
