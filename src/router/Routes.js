import NotificationForm from "../pages/notification/NotificationForm"
import List from "../pages/notification/List"
import { dashboardPath} from "../router/DashboardPath"


export const routes = [
	{
		path: dashboardPath.NOTIFICATIONLIST,
		component: List,
		exact: true,
	},
	{
		path: dashboardPath.NOTIFICATIONFORM,
		component: NotificationForm,
		exact: true,
	}
]