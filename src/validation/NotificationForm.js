import {
  initialValue,
  initialValueError,
  ApplicableTo,
  ApplicableToError,
} from "../constants/notification/notificationForm/Form";

export const formHandleOnchange = (name, error, value) => {
  if (value.length === 0) {
    console.log(name);
    error = { ...error, [name]: "Required" };
  } else {
    if (value.length <= 5 && !Array.isArray(value)) {
      error = { ...error, [name]: "please Enter value above 5" };
    } else {
      error = { ...error, [name]: "" };
    }
  }

  return { ...error };
};

export const mainFormCheckError = (values, isError) => {
  console.log(values);
  for (const item in values) {
    if (values[item].length !== 0) {
      return (isError = true);
    }
  }

  // console.log(values);
  // for (const item in values) {
  //   if (values[item].length <= 0) {
  //     return (isError = true);
  //   }
  //   if (item === "subject" || item === "content") {
  //     if (values[item].length < 5) {
  //       return (isError = true);
  //     }
  //   }
  //   if (
  //     item === "freaquency" ||
  //     item === "increamentTime" ||
  //     item === "notificationFreaquency" ||
  //     item === "ageLimitFrom" ||
  //     item === "ageLimitTo"
  //   ) {
  //     return (isError = true);
  //   }
  // }

  return isError;
};



export const getErrorFromForm = (values) => {
  console.log(values)
  let error = {};
  for (const item in values) {
    if (values[item].length === 0) {
      error = { ...error, [item]: "Required" };
    } else {
      error = { ...error, [item]: "" };
    }

    if (item === "subject" || item === "content") {
      if (Boolean(values[item])) {
        if (values[item].length < 5) {
          error = { ...error, [item]: "please Enter the value above 5" };
        } else {
          error = { ...error, [item]: "" };
        }
      }
    }
    if (
      item === "frequency" ||
      item === "incrementalTime" ||
      item === "notificationPerFreq" 
    ) {
      if (Boolean(values[item])) {
        Number(values[item]) < 0
          ? (error = { ...error, [item]: "negetive value not allowed" })
          : (error = { ...error, [item]: "" });
      }
    }
  }
  return error;
};


export const getApplyToFormError = (values) => {
  console.log(values)
  let error = {};

  for (const item in values) {
    if(item === "ageFrom" ||
    item === "ageTo" ||
    item ==="pincodes" ||
    item ==="userTypes" ||
    item ==="gender" ||
    item ==="plans" ||
    item ==="promotions"){
      if (values[item].length === 0) {
        error = { ...error, [item]: "Required" };
      } else {
        error = { ...error, [item]: "" };
      }
    }
    

    if (
      item === "ageFrom" ||
      item === "ageTo"
    ) {
      if (Boolean(values[item])) {
        Number(values[item]) < 0
          ? (error = { ...error, [item]: "negetive value not allowed" })
          : (error = { ...error, [item]: "" });
      }
    }

    if (item === "ageFrom" || item === "ageTo") {
      if (Boolean(values[item]) && Number(values[item]) > 0) {
        Number(values[item]) > 100
          ? (error = { ...error, [item]: "age limit not allowed to above 100" })
          : (error = { ...error, [item]: "" });
      }
      console.log(error);
    }

    if (item === "ageFrom") {
      if (
        Boolean(values[item]) &&
        Number(values[item]) < 100 &&
        Number(values[item]) > 0
      ) {
        Number(values[item]) < Number(values["ageTo"])
          ? (error = {
              ...error,
              [item]:
                "ageLimitFrom value not allowed to grater than of ageLmitTo value",
            })
          : (error = { ...error, [item]: "" });
      }
    }
  }

  return error;
  
}
