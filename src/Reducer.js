import { combineReducers } from "redux"
import {notificationListReducer} from "./actions/notification/List"

export const reducers = combineReducers({
	notificationListReducer,
})
